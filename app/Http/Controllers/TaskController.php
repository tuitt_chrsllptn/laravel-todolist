<?php

namespace App\Http\Controllers;
use App\Task;

use Illuminate\Http\Request;

class TaskController extends Controller
{
	public function showTasks(){
		$tasklist = Task::all();
		return view("tasks.tasklist", compact('tasklist'));
	}

	public function addTasks(Request $request){
		$newtask = new Task;
		$newtask->name = $request->taskname;
		$newtask->slug = Now()->format('YmdHis'); /*to get the exact date and time*/
		// dd($newtask);
		$newtask->save();
		//redirect us to the previous page
		return redirect('/tasklist');
	}
}
