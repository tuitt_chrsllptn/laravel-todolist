<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel to-do list app</title>

	{{-- Bootstrap CDN --}}

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row justify-content-center text-center">
			<div class="col-md-10 jumbotron">
				<h1 class="display-4"> My task today: {{ Now()->toFormattedDateString() }}  </h1>
				<hr>
				{{--Form to add task  --}}
				<form action="/addtask" method="POST">
					@csrf
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">Task: name:</span>
						</div>
						<input type="text" class="form-control" name="taskname">
						<div class="input-group-append">
							<button class="btn btn-outline-secondary" type="submit">Add</button>
						</div>
					</div>
				</form>
			</div>
		</div>

	{{-- row to retrieve task lists --}}
		<div class="row justify-content-center">
			<div class="col-md-6">
				<ul class="list-group">
					@foreach($tasklist as $task)
					<li class="list-group-item justify-content-between align-items-center">
						<span class="lead">{{ $task->name }}</span>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>




	{{-- Bootstrap script --}}
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>